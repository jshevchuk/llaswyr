//Recommends a set of rosary mysteries according to the day of the week.

package main

import "fmt"
import "time"

func main() {

	switch time.Now().Weekday() {
	case time.Saturday, time.Monday:
    fmt.Println("Recite Joyful Mysteries today.")
    case time.Tuesday, time.Friday:
    fmt.Println("Recite Sorrowful Mysteries today.")
    case time.Wednesday, time.Sunday:
    fmt.Println("Recite Glorious Mysteries today.")
    case time.Thursday:
    fmt.Println("Recite Luminous Mysteries today.")
    default:
    fmt.Println("Say a rosary today.")
    }
}
