#!/usr/bin/env/ ruby

# Recommends a set of rosary mysteries according to the day of the week.

def mystery(int)
  case int
  when 0, 3
    return "Glorious"
  when 1, 6
    return "Joyful"
  when 2, 5
    return "Sorrowful"
  when 4
    return "Luminous"
  else
    return "any"
  end
end

today = Time.new
twday = today.wday

tdaymyst = mystery(twday)

puts "Recite #{tdaymyst} Mysteries today"

