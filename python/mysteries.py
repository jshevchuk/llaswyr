#!usr/bin/env/ python3

#Recommends a set of rosary mysteries according to the day of the week.

import datetime

def which_mystery(wday):
	if wday == 0 or wday == 5:
		mysts = "Joyful"
	elif wday == 1 or wday == 4:
		mysts = "Sorrowful"
	elif wday == 2 or wday == 6:
		mysts = "Glorious"
	elif wday == 3:
		mysts = "Luminous"
	else:
		mysts = "any"
	return mysts

my_today = datetime.datetime.now()
my_weekday = datetime.date.weekday(my_today)
mysteries = which_mystery(my_weekday)

print("Recite " + mysteries + " Mysteries today")