// Recommends a set of rosary mysteries according to the day of the week.

function mysteries(weekday) {

switch (weekday) {
  case 0, 3:
    return "Glorious Mysteries";
    break;
  case 1, 6:
    return "Joyful Mysteries";
    break;
  case 2, 5:
    return "Sorrowful Mysteries";
    break;
  case 4:
    return "Luminous Mysteries";
}
}

var myst = mysteries(new Date().getDay())

console.log("Recite " + myst + " today")