<?php

// Recommends a set of rosary mysteries according to the day of the week.

function tdaymyst($todayweekday) {
	switch ($todayweekday) {
		case 1:
		case 6:
			return "Joyful";
			break;
		case 2:
		case 5:
			return "Sorrowful";
			break;
		case 3:
		case 0:
			return "Glorious";
			break;
		case 4:
			return "Luminous";
			break;
		default:
			return "any";
	}
}

$weekday = date("w");
$mystset = tdaymyst($weekday);

echo "Recite {$mystset} Mysteries today\n";

?>